// Importação de projeto
import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import Routes from "./src/routes";
import { useFonts } from "expo-font";

export default function App() {
  // Carrega as fontes externas instaladas
  const [fontsLoaded] = useFonts({
    PoppinsBlack: require('./assets/fonts/Poppins/Poppins-Black.ttf'),
    PoppinsBold: require('./assets/fonts/Poppins/Poppins-Bold.ttf'),
    PoppinsSemiBold: require('./assets/fonts/Poppins/Poppins-SemiBold.ttf'),
    PoppinsRegular: require('./assets/fonts/Poppins/Poppins-Regular.ttf'),
    PoppinsMedium: require('./assets/fonts/Poppins/Poppins-Medium.ttf'),
    PoppinsLight: require('./assets/fonts/Poppins/Poppins-Light.ttf'),

  });

  // Verifica se todas as fontes foram carregadas antes de renderizar o conteúdo
  if (!fontsLoaded) {
    return null; // Retorna o tipo null como indicador de carregamento
  }
  return (
    <NavigationContainer>
      {/*Renderiza o conteúdo do aplicativo*/}
      <Routes/> 
    </NavigationContainer>
  );
}