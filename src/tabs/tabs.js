// Importação de projeto
import React from 'react';
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";

// Importação das páginas
import Home from './pages/home';
import Profile from './pages/profile';

// Importação dos icones
import { Ionicons } from '@expo/vector-icons'

const Tab = createBottomTabNavigator();

function Tabs() {
    return (
        // Configurações do bottom tabs
        <Tab.Navigator
            screenOptions={{
                tabBarActiveTintColor: 'white',
                tabBarInactiveTintColor: '#f1f1f1',
                tabBarActiveBackgroundColor: '#2E688C',
                tabBarInactiveBackgroundColor: '#2E688C',
                tabBarShowLabel: false,
                tabBarStyle: {
                    paddingTop: '20px',
                    backgroundColor: '#2E688C'
                }

            }}
        >
            {/*Página home*/}
            <Tab.Screen
                name="Home"
                component={Home}
                options={{
                    headerShown: false,
                    tabBarIcon: ({ color, size, focused }) => {
                        if (focused) {
                            return <Ionicons name="home" size={size} color={color} />
                        }

                        return <Ionicons name="home-outline" size={size} color={color} />
                    }
                }}
            />
            {/*Página do perfil*/}
            <Tab.Screen
                name="Profile"
                component={Profile}
                options={{
                    headerShown: false,
                    tabBarIcon: ({ color, size, focused }) => {
                        if (focused) {
                            return <Ionicons name="person-circle-outline" size={size} color={color} />
                        }

                        return <Ionicons name="person-circle-sharp" size={size} color={color} />
                    }
                }}
            />
        </Tab.Navigator>
    );
}

export default Tabs;