import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Platform,
  Image,
  FlatList,
  Modal,
  Pressable,
  Alert,
} from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";
import Ionicons from "react-native-vector-icons/Ionicons";
import AsyncStorage from "@react-native-async-storage/async-storage";
import sheets from "../../../axios/axios";
import { useFocusEffect } from "@react-navigation/native";

// Define uma lista de categorias que serão exibidas na tela
const categories = [
  { title: "Ciências" },
  { title: "Artes" },
  { title: "Literatura" },
  { title: "Tecnologia" },
  { title: "Infantil" },
];

// Componente principal da tela Home
export default function Home({ navigation }) {
  // Estados para armazenar os livros populares, visibilidade do modal, IDs de usuário e livro, data atual e data após 14 dias
  const [popularBooks, setPopularBooks] = useState([]);
  const [modalVisible, setModalVisible] = useState(false);
  const [idUsuario, setIdUsuario] = useState("");
  const [idLivro, setIdLivro] = useState("");
  const [currentDate, setCurrentDate] = useState(new Date());
  const [dateAfter14Days, setDateAfter14Days] = useState(null);

  // useEffect para buscar livros disponíveis quando o componente é montado
  useEffect(() => {
    fetchAvailableBooks();
  }, []);

  // useFocusEffect para buscar livros disponíveis quando a tela ganha foco
  useFocusEffect(
    React.useCallback(() => {
      fetchAvailableBooks();
    }, [])
  );

  // Função para criar uma reserva de livro
  const createSchedule = async () => {
    try {
      // Formata as datas para o formato ISO (YYYY-MM-DD)
      const dataReservaFormatada = currentDate.toISOString().split("T")[0];
      const dataExpiracaoFormatada = dateAfter14Days.toISOString().split("T")[0];
  
      // Envia os dados da reserva para a API
      const response = await sheets.postReserva({
        idUsuario,
        idLivro,
        dataReserva: dataReservaFormatada,
        dataExpiracao: dataExpiracaoFormatada,
      });
  
      // Verifica a resposta da API
      if (response.status === 201) {
        Alert.alert("Reserva cadastrada com sucesso");
        // setDateAfter14Days(dateAfter14Days); // Define a data de expiração
        // Fecha o modal e atualiza a lista de livros após 1 segundo
        setTimeout(() => {
          setModalVisible(false);
          fetchAvailableBooks();
        }, 1000);
      } else {
        console.error("Erro ao criar agendamento:", response.data.error);
      }
    } catch (error) {
      Alert.alert(error.response.data.error);
      console.log("Erro ao criar agendamento:", error);
    }
  };

  // Função para buscar os livros disponíveis na API
  const fetchAvailableBooks = async () => {
    try {
      // Faz a requisição para a API
      const response = await sheets.getBooksAvailability();
      if (response.status === 200) {
        const booksData = response.data.getBooks;
        if (Array.isArray(booksData)) {
          // Converte a resposta em um array de livros
          const booksArray = booksData.reduce(
            (accumulator, currentValue) => accumulator.concat(currentValue),
            []
          );
          // Formata os dados dos livros para serem exibidos
          const formattedBooks = booksArray.map((book) => ({
            id: book.idLivro,
            image: book.imageUrl,
            title: book.titulo,
            author: book.autor,
            status: book.disponibilidade,
          }));
          setPopularBooks(formattedBooks);
        } else {
          console.error("Dados da resposta não são um array:", booksData);
        }
      } else {
        console.error(
          "Erro ao buscar livros disponíveis:",
          response.data.error
        );
      }
    } catch (error) {
      console.error("Erro ao buscar livros disponíveis:", error);
    }
  };

  // Função para abrir o modal e definir os dados do livro e usuário
  const handleBookPress = async (item) => {
    setModalVisible(true); // Abre o modal
    const userDataString = await AsyncStorage.getItem("userData"); // Obtém os dados do usuário do armazenamento local
    const userData = JSON.parse(userDataString); // Converte os dados do usuário de string para objeto
    const userId = parseInt(userData.user.idUsuario); // Obtém o ID do usuário
    setIdUsuario(userId); // Define o ID do usuário no estado
    setIdLivro(item.id); // Define o ID do livro no estado
    
    // Define a data de expiração ao abrir o modal
    const currentDate = new Date();
    const dateAfter14Days = new Date();
    dateAfter14Days.setDate(currentDate.getDate() + 14);
    setDateAfter14Days(dateAfter14Days);
  };

  // Função para fechar o modal
  const handleCloseModal = () => {
    setModalVisible(false);
  };

  return (
    <SafeAreaView style={styles.container}>
      {/* Modal para reservar um livro */}
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={handleCloseModal}
      >
        <View style={styles.modalContainer}>
          <View style={styles.modalContent}>
            <Pressable style={styles.closeButton} onPress={handleCloseModal}>
              <Ionicons name="close-outline" size={24} color="#202020" />
            </Pressable>

            <Text style={styles.modalTitle}>Reserva de Livro</Text>

            {/* Exibe a data de reserva e data de retorno */}
            <View style={{ marginTop: 10, marginBottom: 10 }}>
              <Text>
                <Text
                  style={{
                    fontFamily: "PoppinsRegular",
                    fontSize: 20,
                    marginBottom: 10,
                  }}
                >
                  Data de Reserva: {currentDate.toISOString().split("T")[0]}
                </Text>
              </Text>
              {dateAfter14Days && (
                <Text style={{ fontFamily: "PoppinsRegular", fontSize: 20 }}>
                  Data de Retorno: {dateAfter14Days.toISOString().split("T")[0]}
                </Text>
              )}
            </View>

            {/* Botão para confirmar a reserva */}
            <View style={styles.modalButtons}>
              <TouchableOpacity
                style={styles.modalButton}
                onPress={createSchedule}
              >
                <Text style={styles.buttonText}>Reservar</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>

      {/* Cabeçalho da tela */}
      <View style={styles.header}>
        <Text style={styles.logo}>Library Z</Text>
        <TouchableOpacity
          style={styles.profileButton}
          onPress={() => navigation.navigate("Profile")}
        >
          <Ionicons name="person-circle-outline" size={30} color="#202020" />
        </TouchableOpacity>
      </View>

      {/* Barra de pesquisa */}
      <View style={styles.searchBar}>
        <Ionicons
          name="search"
          size={20}
          color="#ccc"
          style={{ marginRight: 10 }}
        />
        <TextInput style={styles.searchInput} placeholder="Pesquisar livros" />
      </View>

      {/* Conteúdo principal da tela */}
      <View style={styles.content}>
        <Text style={styles.sectionTitle}>Categorias</Text>
        <View style={styles.categoriesContainer}>
          {categories.map((category, index) => (
            <Pressable key={index}>
              <Text key={index} style={styles.category}>
                {category.title}
              </Text>
            </Pressable>
          ))}
        </View>

        <Text style={styles.sectionTitle}>Populares</Text>
        <FlatList
          data={popularBooks}
          renderItem={({ item }) => (
            <TouchableOpacity onPress={() => handleBookPress(item)}>
              <View style={styles.bookCard}>
                <Image source={{ uri: item.image }} style={styles.bookImage} />
                <View style={styles.bookInfo}>
                  <Text style={styles.bookTitle}>{item.title}</Text>
                  <Text style={styles.bookAuthor}>{item.author}</Text>
                </View>
              </View>
            </TouchableOpacity>
          )}
          keyExtractor={(item, index) => index.toString()}
        />
      </View>
    </SafeAreaView>
  );
}

// Configurações de estilização
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#AED3F2",
    paddingHorizontal: 20,
    paddingTop: Platform.OS === "ios" ? 60 : 40,
  },
  header: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginBottom: 20,
  },
  logo: {
    fontSize: 22,
    fontFamily: "PoppinsBold",
    color: "#333",
  },
  profileButton: {},
  searchBar: {
    flexDirection: "row",
    alignItems: "center",
    backgroundColor: "#f0f0f0",
    borderWidth: 0,
    borderColor: "#e0e0e0",
    borderRadius: 10,
    paddingHorizontal: 10,
    marginBottom: 15,
    height: 50,
  },
  searchInput: {
    flex: 1,
    fontFamily: "PoppinsRegular",
    paddingVertical: 8,
  },
  content: {
    flex: 1,
  },
  sectionTitle: {
    fontSize: 20,
    fontFamily: "PoppinsMedium",
    marginBottom: 10,
    color: "#333",
  },
  categoriesContainer: {
    flexDirection: "row",
    flexWrap: "wrap",
    marginBottom: 15,
  },
  category: {
    fontSize: 16,
    fontFamily: "PoppinsLight",
    marginBottom: 5,
    color: "#555",
    marginRight: 10,
  },
  bookCard: {
    backgroundColor: "#f8f8f8",
    marginBottom: 15,
    padding: 15,
    borderRadius: 10,
    flexDirection: "row",
    alignItems: "center",
    ...Platform.select({
      ios: {
        shadowColor: "#000",
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.2,
        shadowRadius: 3,
      },
    }),
  },
  bookImage: {
    width: 70,
    height: 100,
    resizeMode: "cover",
    borderRadius: 5,
    marginRight: 15,
  },
  bookInfo: {
    flex: 1,
  },
  bookTitle: {
    fontSize: 16,
    fontFamily: "PoppinsSemiBold",
    color: "#202020",
  },
  bookAuthor: {
    fontSize: 14,
    fontFamily: "PoppinsRegular",
    color: "#555",
  },
  borrowedDate: {
    fontSize: 12,
    color: "#777",
  },
  modalContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22,
  },
  modalContent: {
    width: "80%",
    backgroundColor: "#F2F2F2",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    ...Platform.select({
      ios: {
        shadowColor: "#000",
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.2,
        shadowRadius: 3,
      },
    }),
  },
  modalTitle: {
    marginBottom: 15,
    textAlign: "center",
    fontSize: 28,
    fontFamily: "PoppinsBold",
    color: "#202020",
  },
  modalBookTitle: {
    textAlign: "center",
    fontSize: 20,
    fontFamily: "PoppinsMedium",
    color: "#202020",
  },
  modalButton: {
    backgroundColor: "#4586BF",
    borderRadius: 30,
    paddingVertical: 15,
    paddingHorizontal: 10,
    width: 150,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    ...Platform.select({
      ios: {
        shadowColor: "#000",
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.2,
        shadowRadius: 3,
      },
    }),
  },
  modalButtons: {
    paddingTop: 10,
  },
  buttonText: {
    color: "#fff",
    fontSize: 18,
    fontFamily: "PoppinsMedium",
  },
  closeButton: {
    position: "absolute",
    top: 10,
    right: 10,
  },
});
