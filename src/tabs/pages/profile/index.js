import React, { useEffect, useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  FlatList,
  Pressable,
  Modal,
  Button,
  Platform,
} from "react-native";
import { TextInput } from "react-native-gesture-handler";
import Ionicons from "react-native-vector-icons/Ionicons";
import AsyncStorage from "@react-native-async-storage/async-storage";
import sheets from "../../../axios/axios";
import { useFocusEffect } from "@react-navigation/native";

// Componente principal da tela Profile
export default function Profile({ navigation }) {
  // Estado para armazenar os dados do usuário
  const [userData, setUserData] = useState(null);
  // Estado para controlar a visibilidade do modal
  const [modalVisible, setModalVisible] = useState(false);
  // Estado para armazenar os livros reservados pelo usuário
  const [borrowedBooks, setBorrowedBooks] = useState([]);
  
  // Estados para os campos de atualização do perfil
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  // Função para atualizar os dados do usuário
  const handleUpdate = async () => {
    try {
      if (!userData) return;

      // Dados do usuário atualizados
      const updatedUser = { name, email, password };
      const response = await sheets.updateUser(userData.user.idUsuario, updatedUser);

      const clear = () => {
        setName("");
        setEmail("");
        setPassword("");
      };

      // Verifica a resposta da API
      if (response.status === 200) {
        // Atualiza o estado com os novos dados do usuário
        const newUserData = {
          ...userData,
          user: {
            ...userData.user,
            name,
            email,
            password,
          },
        };
        setUserData(newUserData);
        // Armazena os novos dados do usuário no AsyncStorage
        await AsyncStorage.setItem("userData", JSON.stringify(newUserData));
        clear();
        setModalVisible(false);
      }
    } catch (error) {
      // console.error("Erro ao atualizar o usuário:", error);
    }
  };

  // Função para buscar os livros reservados pelo usuário
  const fetchReservedBooks = async (userId) => {
    try {
      const response = await sheets.getReservedBooksByUser(userId);
      const reservedBooksData = response.data.reservedBooks[0];
      setBorrowedBooks(reservedBooksData);
    } catch (error) {
      // console.error('Erro ao buscar livros reservados:', error);
    }
  };

  // Função que armazena outras funções para usar no onPress
  const handlePress = () => {
    handleUpdate();
    setModalVisible(!modalVisible);
  };

  // Função que fecha o modal
  const handleCloseModal = () => {
    setModalVisible(false);
  };

  // Função de logout
  const handleLogout = async () => {
    try {
      // Remove os dados do usuário do AsyncStorage
      await AsyncStorage.removeItem("userData");
      // Navega para a tela inicial
      navigation.navigate("Init");
    } catch (error) {
      console.error("Error logging out:", error);
    }
  };

  // Função para devolver um livro
  const returnBook = async (idReserva) => { 
    try {
      // Chama a função para deletar a reserva e o livro associado a ela
      const response = await sheets.deleteSchedule(idReserva);
      if (response.status === 200) {
        // Atualiza o estado dos livros reservados após a remoção do livro
        setBorrowedBooks(prevBooks => prevBooks.filter(book => book.idReserva !== idReserva));
      } else {
        console.error('Erro ao deletar a reserva:', response.error);
      }
    } catch (error) {
      console.error('Erro ao devolver o livro:', error);
    }
  };

  // Efeito para buscar os dados do usuário e dos livros reservados toda vez que a tela ganhar foco
  useFocusEffect(
    React.useCallback(() => {
      const retrieveUserData = async () => {
        try {
          const storedUserData = await AsyncStorage.getItem("userData");
          if (storedUserData) {
            const parsedData = JSON.parse(storedUserData);
            setUserData(parsedData);
            const userId = parsedData.user.idUsuario;
            fetchReservedBooks(userId);
          }
        } catch (error) {
          console.error("Erro ao recuperar dados do usuário:", error);
        }
      };
      retrieveUserData();
    }, [])
  );

  // Caso os dados do usuário não sejam carregados, não mostra nada
  // Se os dados forem carregados, mostra a página do perfil
  if (userData != null) {
    return (
      <View style={styles.container}>
        {/* Modal para atualizar o perfil do usuário */}
        <Modal
          animationType="slide"
          transparent={true}
          visible={modalVisible}
          onRequestClose={() => {
            setModalVisible(!modalVisible);
          }}
        >
          <View style={styles.centeredView}>
            <View style={styles.modalView}>
              <Pressable style={styles.closeButton} onPress={handleCloseModal}>
                <Ionicons name="close-outline" size={24} color="#202020" />
              </Pressable>
              <Text style={styles.modalTitle}>Atualize seu perfil</Text>
              <TextInput
                style={styles.input}
                placeholder="Nome"
                value={name}
                onChangeText={setName}
                keyboardType="default"
                autoCapitalize="none"
              />
              <TextInput
                style={styles.input}
                placeholder="Email"
                value={email}
                onChangeText={setEmail}
                keyboardType="email-address"
                autoCapitalize="none"
                autoCompleteType="email"
              />
              <TextInput
                style={styles.input}
                placeholder="Senha"
                secureTextEntry
                value={password}
                onChangeText={setPassword}
              />
              <Pressable
                onPress={() => handlePress()}
                title="OK"
                color="#3e945a"
                style={styles.button}
              >
                <Text style={styles.modalButtonText}>Atualizar</Text>
              </Pressable>
            </View>
          </View>
        </Modal>

        {/* Cabeçalho com as informações do usuário */}
        <View style={styles.header}>
          {/* Imagem do perfil do usuário (comentada) */}
          {/* <Image
            source={require("../../../../assets/img/euller.jpeg")}
            style={styles.profileImage}
          /> */}
          <View style={styles.profileInfo}>
            <Text style={styles.profileName}>{userData.user.name}</Text>
            <Text style={styles.profileLocation}>Franca-SP</Text>
          </View>
        </View>

        {/* Botões interativos para o usuário */}
        <View style={styles.buttons}>
          <Pressable
            style={styles.button}
            onPress={() => setModalVisible(true)}
          >
            <Text style={styles.buttonText}>Atualizar perfil</Text>
          </Pressable>

          <Pressable style={styles.button} onPress={() => handleLogout()}>
            <Ionicons
              name="log-out-outline"
              size={20}
              color="#202020"
              style={styles.buttonLogout}
            />
            <Text style={styles.buttonText}>Sair</Text>
          </Pressable>
        </View>

        {/* Lista de livros reservados pelo usuário */}
        <View style={styles.content}>
          <Text style={styles.sectionTitle}>Livros reservados:</Text>
          <FlatList
            data={borrowedBooks}
            renderItem={({ item }) => (
              <View style={styles.bookCard}>
                <Image source={{ uri: item.imageUrl }} style={styles.bookImage} />
                <View style={styles.bookInfo}>
                  <Text style={styles.bookTitle}>{item.titulo}</Text>
                  <Text style={styles.bookAuthor}>{item.autor}</Text>
                  {/* <Text style={styles.borrowedDate}>
                    Data de Devolução: {item.dataExpiracao}
                  </Text> */}
                  <View style={{ borderRadius: 30, overflow: "hidden", marginTop: 10 }}>
                    <Button
                      title="Devolver Livro"
                      color="#4586BF"
                      onPress={() => returnBook(item.idReserva, item.title)}
                    />
                  </View>
                </View>
              </View>
            )}
            keyExtractor={(item, index) => index.toString()}
          />
        </View>
      </View>
    );
  } else {
    return <View></View>;
  }
}

// Configurações de estilização
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#AED3F2",
    paddingHorizontal: 20,
    paddingTop: Platform.OS === "ios" ? 100 : 40,
    paddingLeft: Platform.OS === "ios" ? 30 : 40,
  },
  header: {
    flexDirection: "row",
    alignItems: "center",
    marginBottom: 40,
  },
  profileImage: {
    width: 125,
    height: 125,
    borderRadius: 100,
  },
  profileInfo: {
    paddingLeft: Platform.OS === "ios" ? 20 : 20,
    marginTop: 50,
  },
  profileName: {
    fontSize: 24,
    fontFamily: "PoppinsMedium",
    color: "#202020",
  },
  profileLocation: {
    fontSize: 18,
    fontFamily: "PoppinsRegular",
    color: "#555",
  },
  buttons: {
    flexDirection: "row",
    justifyContent: "space-around",
    paddingLeft: Platform.OS === "ios" ? 0 : 0,
    marginBottom: 30,
  },
  button: {
    backgroundColor: "#4586BF",
    borderRadius: 30,
    paddingVertical: 10,
    paddingHorizontal: 10,
    width: 150,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    ...Platform.select({
      ios: {
        shadowColor: "#000",
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.2,
        shadowRadius: 3,
      },
    }),
  },
  buttonText: {
    color: "#202022",
    fontSize: 18,
    fontFamily: "PoppinsMedium",
    alignSelf: "center",
  },
  buttonLogout: {
    marginRight: 25,
    marginLeft: 10,
  },
  content: {
    flex: 1,
  },
  sectionTitle: {
    fontSize: 20,
    fontFamily: "PoppinsMedium",
    marginBottom: 10,
    color: "#333",
  },

  // Seção dos livros
  bookCard: {
    backgroundColor: "#f8f8f8",
    marginBottom: 15,
    padding: 15,
    borderRadius: 10,
    flexDirection: "row",
    alignItems: "center",
    ...Platform.select({
      ios: {
        shadowColor: "#000",
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.2,
        shadowRadius: 3,
      },
    }),
  },
  bookImage: {
    width: 70,
    height: 100,
    resizeMode: "cover",
    borderRadius: 5,
    marginRight: 15,
  },
  bookInfo: {
    flex: 1,
  },
  bookTitle: {
    fontSize: 16,
    fontFamily: "PoppinsSemiBold",
    color: "#202020",
  },
  bookAuthor: {
    fontSize: 14,
    fontFamily: "PoppinsRegular",
    color: "#555",
  },
  borrowedDate: {
    fontSize: 12,
    color: "#777",
    marginBottom: 10,
  },

  //Modal
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22,
  },
  modalView: {
    width: "80%",
    // margin: 20,
    backgroundColor: "#F2F2F2",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    // elevation: 5,
    ...Platform.select({
      ios: {
        shadowColor: "#000",
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.2,
        shadowRadius: 3,
      },
    }),
  },
  modalTitle: {
    marginBottom: 15,
    textAlign: "center",
    fontSize: 20,
    fontFamily: "PoppinsBold",
    color: "#202020",
  },
  modalButtonText: {
    color: "#fff",
    fontSize: 20,
    fontFamily: "PoppinsMedium",
    alignSelf: "center",
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center",
    fontSize: 16,
  },
  input: {
    width: "100%",
    height: 55,
    backgroundColor: "#FFF",
    borderWidth: 0,
    marginBottom: 10,
    paddingHorizontal: 10,
    borderRadius: 10,
    fontFamily: "PoppinsLight",
    color: "#202020",
  },
  closeButton: {
    position: "absolute",
    top: 10,
    right: 10,
  },
});
