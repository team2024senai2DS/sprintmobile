// Importações de projeto
import React, { useState } from 'react';
import { View, Text, TextInput, StyleSheet, Platform, Pressable, Alert } from 'react-native';
import sheets from "../../../axios/axios"

const Cadastro = ({ navigation }) => {
    // Defina os estados para os campos do formulário e a mensagem de erro
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [errorMessage, setErrorMessage] = useState('');

    // Função assíncrona para lidar com o cadastro do usuário
    const handleSignup = async () => {
        try {
            // Realize a chamada à função createUser do módulo 'sheets', passando os dados do usuário
            const response = await sheets.createUser({ name, email, password });

            // Verifique se a resposta da requisição foi bem-sucedida
            if (response.status === 201) {
                // Limpe os campos do formulário
                setName('');
                setEmail('');
                setPassword('');
                // Exiba um alerta informando que o cadastro foi realizado com sucesso
                Alert.alert('Cadastro realizado com sucesso');
                // Navegue para a próxima tela após o cadastro
                navigation.navigate('Login');
            } else {
                // Se a requisição falhar, exiba uma mensagem de erro
                setErrorMessage('Erro ao realizar o cadastro');
            }
        } catch (error) {
            // Em caso de erro, exiba a mensagem de erro adequada
            setErrorMessage(error.response.data.error);
        }
    };

    return (
        <View style={styles.container}>
            <Text style={styles.title}>Faça seu cadastro</Text>

            {/* Area do Formulário */}
            <View style={styles.formContainer}>

                {/* Campo de nome */}
                <TextInput
                    style={styles.input}
                    placeholder="Nome"
                    value={name}
                    onChangeText={setName}
                    keyboardType="default"
                    autoCapitalize="none"
                />

                {/* Campo de email*/}
                <TextInput
                    style={styles.input}
                    placeholder="Email"
                    value={email}
                    onChangeText={setEmail}
                    keyboardType="email-address"
                    autoCapitalize="none"
                    autoCompleteType="email"
                />

                {/* Campo de senha */}
                <TextInput
                    style={styles.input}
                    placeholder="Senha"
                    secureTextEntry
                    value={password}
                    onChangeText={setPassword}
                />

                {/* Botão para realizar o cadastro*/}
                <Pressable onPress={handleSignup} style={styles.button}>
                    <Text style={styles.buttonText}>Cadastrar-se</Text>
                </Pressable>

            </View>

            {errorMessage ? <Text style={styles.errorMessage}>{errorMessage}</Text> : null}
        </View>
    );
};

// Configurações de estilização
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: '#AED3F2',
        alignItems: 'center',
        paddingHorizontal: 20,
    },
    title: {
        fontSize: 38,
        fontFamily: 'PoppinsBold',
        marginBottom: 30,
        color: '#202022', // Cor de título mais escura
    },
    formContainer: {
        width: '100%',
        backgroundColor: '#F2F2F2',
        padding: 25,
        borderRadius: 10,
        marginBottom: 15,
        ...Platform.select({
            ios: {
                shadowColor: '#000',
                shadowOffset: { width: 0, height: 2 },
                shadowOpacity: 0.2,
                shadowRadius: 3,
            }
        })
    },
    textAddress: {
        fontSize: 16,
        fontFamily: 'PoppinsRegular',
        paddingBottom: 10,
    },
    input: {
        width: '100%',
        height: 55,
        backgroundColor: '#FFF',
        borderWidth: 0,
        marginBottom: 10,
        paddingHorizontal: 10,
        borderRadius: 10,
        fontFamily: 'PoppinsLight',
        color: '#202022'
    },
    button: {
        backgroundColor: '#4586BF',
        borderRadius: 8,
        padding: 10,
        width: '80%',
        marginTop: 10,
        alignSelf: 'center',
        ...Platform.select({
            ios: {
                shadowColor: '#000',
                shadowOffset: { width: 0, height: 2 },
                shadowOpacity: 0.2,
                shadowRadius: 3,
            },
        }),
    },
    buttonText: {
        color: '#fff',
        fontSize: 20,
        fontFamily: 'PoppinsMedium',
        alignSelf: 'center',
    },
    errorMessage: {
        fontFamily: 'PoppinsBold',
        color: 'red',
        fontSize: 18,
        marginTop: 10,
    },
});

export default Cadastro;

