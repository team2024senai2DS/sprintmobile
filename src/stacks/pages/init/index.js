// Importações de projeto
import React from 'react';
import { View, Text, Pressable, StyleSheet, Image, Platform } from 'react-native';

export default function InitScreen({ navigation }) {
  return (
    <View style={styles.container}>
      <View style={styles.overlay}>
        <Image source={require('../../../../assets/img/livro.png')} style={styles.Image1} />
        <View style={styles.header}>
          <Text style={styles.title}>Bem-vindo ao Library Z</Text>
        </View>
        <View style={styles.content}>
          <Text style={styles.optionText}>Selecione uma opção:</Text>
          <Pressable style={styles.button} onPress={() => navigation.navigate('Login')}>
            <Text style={styles.buttonText}>Login</Text>
          </Pressable>
          <Pressable style={styles.button} onPress={() => navigation.navigate('Cadastro')}>
            <Text style={styles.buttonText}>Cadastrar</Text>
          </Pressable>
        </View>
      </View>
    </View>
  );
}

// Configurações de estilização
const styles = StyleSheet.create({

  container: {
    flex: 1,
    backgroundColor: '#AED3F2',
    paddingHorizontal: 20,
    paddingTop: Platform.OS === 'ios' ? 120 : 60,

  },
  header: {
    marginBottom: 20,
  },
  Image1: {
    height: 250,
    width: 200,
    borderRadius: 20,
    position: "absolute",
    top: 10,
    transform: [
      { translateX: 70 },
      { translateY: 40 },

    ]
  },
  title: {
    fontSize: 30,
    fontWeight: 'bold',
    textAlign: 'left',
    color: '#202022', 
    marginTop: 400,
    fontFamily: 'PoppinsBold'
  },
  content: {
    alignItems: 'left',
  },
  optionText: {
    fontSize: 20,
    fontFamily: 'PoppinsMedium',
    color: '#202022', 
    marginBottom: 20,
  },
  button: {
    backgroundColor: "#4586BF",
    borderRadius: 8,
    padding: 15,
    width: '100%',
    marginBottom: 10,
    alignSelf: 'center',
    ...Platform.select({
      ios: {
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.2,
        shadowRadius: 3,
      }
    })
  },
  buttonText: {
    fontSize: 20,
    color: '#fff',
    fontFamily: 'PoppinsMedium',
    alignSelf: 'center'
  },
});
