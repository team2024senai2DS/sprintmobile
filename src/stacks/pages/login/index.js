import React, { useState } from 'react';
import { View, Text, TextInput, StyleSheet, Platform, Pressable, Alert } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import sheets from '../../../axios/axios'; // Importação do módulo axios personalizado, certifique-se de que o caminho esteja correto

const Login = ({ navigation }) => {
  // Definição dos estados para o email, senha e mensagem de erro
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [errorMessage, setErrorMessage] = useState('');

  // Função assíncrona para lidar com o processo de login
  const handleLogin = async () => {
    try {
      // Requisição para o servidor utilizando o módulo axios personalizado
      const response = await sheets.signIn({ email, password });
      // Extração dos dados de usuário da resposta
      const userData = response.data;

      // console.log(userData);

      // Limpeza dos campos de email e senha
      setEmail('');
      setPassword('');
      
      // Exibição de um alerta de login bem-sucedido
      Alert.alert('Login realizado com sucesso');
      
      // Armazenamento dos dados do usuário localmente
      await AsyncStorage.setItem('userData', JSON.stringify(userData));
      
      // Navegação para a tela "Tabs" após o login
      navigation.navigate('Tabs');
    } catch (error) {
      // Tratamento de erros, exibição da mensagem de erro adequada
      setErrorMessage('Credenciais ou usuário inválidos. Por favor, tente novamente.');
    }
  };

  // Estrutura do componente de login
  return (
    <View style={styles.container}>
      <Text style={styles.title}>Login</Text>
      <View style={styles.formContainer}>
        {/* Campo de entrada para o email */}
        <TextInput
          style={styles.input}
          placeholder="Email"
          value={email}
          onChangeText={setEmail}
          keyboardType="email-address"
          autoCapitalize="none"
          autoCompleteType="email"
        />
        {/* Campo de entrada para a senha */}
        <TextInput
          style={styles.input}
          placeholder="Senha"
          secureTextEntry
          value={password}
          onChangeText={setPassword}
        />
        {/* Botão para realizar o login */}
        <Pressable onPress={handleLogin} style={styles.button}>
          <Text style={styles.buttonText}>Login</Text>
        </Pressable>
      </View>
      {/* Exibição da mensagem de erro, se houver */}
      {errorMessage ? <Text style={styles.errorMessage}>{errorMessage}</Text> : null}
    </View>
  );
};

// Estilos para os componentes do login
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#AED3F2',
    alignItems: 'center',
    paddingHorizontal: 20,
  },
  title: {
    fontSize: 38,
    fontFamily: 'PoppinsBold',
    marginBottom: 30,
    color: '#202022',
  },
  formContainer: {
    width: '100%',
    backgroundColor: '#F2F2F2',
    padding: 25,
    borderRadius: 10,
    marginBottom: 15,
    ...Platform.select({
      ios: {
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.2,
        shadowRadius: 3,
      }
    })
  },
  input: {
    width: '100%',
    height: 55,
    backgroundColor: '#FFF',
    borderWidth: 0,
    marginBottom: 10,
    paddingHorizontal: 10,
    borderRadius: 10,
    fontFamily: 'PoppinsLight',
    color: '#202022'
  },
  button: {
    backgroundColor: '#4586BF',
    borderRadius: 8,
    padding: 10,
    width: '80%',
    marginTop: 10,
    alignSelf: 'center',
    ...Platform.select({
      ios: {
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.2,
        shadowRadius: 3,
      },
    }),
  },
  buttonText: {
    color: '#fff',
    fontSize: 20,
    fontFamily: 'PoppinsMedium',
    alignSelf: 'center',
  },
  errorMessage: {
    fontFamily: 'PoppinsBold',
    color: 'red',
    fontSize: 18,
    marginTop: 10,
  },
});

export default Login; // Exportação do componente Login
