import axios from "axios";

const api = axios.create({
  baseURL: "http://10.89.234.164:5000/booksApi/",
  headers: {
    Accept: "application/json",
  },
});

const sheets = {
    //criar reserva
    postReserva: (reserva) => api.post("schedule/createSchedule/", reserva),

    //login
    signIn: (user) => api.post("auth/signIn/", user),

    //cadastro
    createUser: (user) => api.post("user/createUser/", user),

    //updateUser
    updateUser: (idUsuario, user) => api.put(`user/updateUser/${idUsuario}`, user),

    //listar livros disponiveis
    getBooksAvailability: () => api.get("book/getBooksAvailability/"),

    //listar livros reservados pelo usuario
    getReservedBooksByUser: (idUsuario) => api.get(`schedule/getReservedBooksByUser/${idUsuario}`),

    //Deletar Reserva
    deleteSchedule: (idReserva) => api.delete(`schedule/deleteSchedule/${idReserva}`),
};

export default sheets;
