// Importações de projeto
import React from 'react'
import { createStackNavigator } from "@react-navigation/stack";

// Importações de páginas
import InitScreen from './stacks/pages/init';
import Login from './stacks/pages/login';
import Tabs from './tabs/tabs';
import Cadastro from './stacks/pages/signup';

const Stack = createStackNavigator();

const Routes = () => {
  return (
    // Define a rota inicial e as configurações da stack
      <Stack.Navigator initialRouteName="Init" screenOptions={{ headerShown: false}}>
        {/*Página Inicial*/}
        <Stack.Screen name="Init" component={InitScreen} />
        {/*Página de login*/}
        <Stack.Screen name="Login" component={Login} />
        {/*Página de cadastro*/}
        <Stack.Screen name="Cadastro" component={Cadastro} />
        {/*Redireciona para a seção de Tabs*/}
        <Stack.Screen name="Tabs" component={Tabs} />
      </Stack.Navigator>
  )
}

export default Routes;