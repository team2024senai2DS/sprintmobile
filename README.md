# Sprint Mobile

> Este aplicativo tem como objetivo simplificar o processo de empréstimo e reserva de livros em bibliotecas e espaços de leitura.

### Ajustes e melhorias

O projeto ainda está em desenvolvimento e as próximas atualizações serão voltadas nas seguintes tarefas:

- [x] Criar o app usando Expo-RN
- [x] Criar a página inicial
- [x] Criar a página de login
- [x] Criar a página de cadastro
- [x] Criar a página de inicial pós-login
- [x] Efetuar a conexão com a API externa
- [x] Criar lógica para armazenar os dados do usuário pós-login
- [x] Criar função para reservar os livros fictícios
- [ ] Refatorar código para melhor execução

## 💻 Pré-requisitos

- Node.js
- Android Studio

**Passos:**

1. **Instalar o Node.js:** Acesse o site oficial <https://nodejs.org/en> e baixe a versão mais recente para o seu sistema operacional.

2. **Instalar o Android Studio:** Acesso o site oficial <https://developer.android.com/studio?hl=pt-br> e baixe a versão mais recente para o seu sistema operacional

## 🚀 Instalando Sprint Mobile

Para instalar o Sprint Mobile, siga estas etapas:

1. **Clonar o projeto:** Utilize o comando abaixo para clonar o projeto para o seu computador.

```bash
git clone https://gitlab.com/team2024senai2DS/sprintmobile
```

2. **Instalar as dependências:** Navegue até a pasta do projeto e execute o comando:

```bash
npm install
```

Com isto as dependências serão instaladas.

## ☕ Usando Sprint Mobile

Para usar a Sprint API, siga estas etapas:

**Iniciar o aplicativo usando o Expo:**

1. Execute o comando `npx expo start` no terminal.
2. O aplicativo estará disponível na porta após você conectar algum dispostivo móvel.

**Consumir a API externa**

Você pode consultar e ver o passo a passo para usa-lá nesse link: <https://gitlab.com/team2024senai2DS/sprintapi>

## 📫 Contribuindo para o projeto

Contribuições são sempre bem-vindas!

Veja o [CONTRIBUINDO](CONTRIBUTING) para saber como começar.

## 🤝 Colaboradores

<table>
  <tr>
    <td align="center">
      <a href="https://github.com/igordmouraa">
        <img src="https://avatars.githubusercontent.com/u/127807075" width="100px;" alt="Foto do Igor Moura no GitHub"/><br>
        <sub>
          <b>Igor Moura</b>
        </sub>
      </a>
    </td>
        <td align="center">
      <a href="https://gitlab.com/gabrielazevedo1222">
        <img src="https://gitlab.com/uploads/-/system/user/avatar/13628288/avatar.png?width=800" width="100px;" alt="Foto do Gabriel Azevedo no GitLab"/><br>
        <sub>
          <b>Gabriel Azevedo Gonçalves</b>
        </sub>
      </a>
    </td>
        <td align="center">
      <a href="https://gitlab.com/vinizamara">
        <img src="https://secure.gravatar.com/avatar/a043cc90e5fdf5c1eccb44b99bd596ebc7c4ca2fa547aef9bea7389c5031d629?s=1600&d=identicon" width="100px;" alt="Foto do Vinicius Manfrin no GitHub"/><br>
        <sub>
          <b>Vinicius Manfrin Zamara</b>
        </sub>
      </a>
    </td>
    
  </tr>
</table>

## 📝 Licença

Esse projeto está sob licença. Veja o arquivo [LICENÇA](LICENSE) para mais detalhes.
